# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    str.delete!(ch) if ch == ch.downcase
  end
  str
  #str.chars.select {|letter| letter == letter.upcase}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle_index = str.length / 2
  if str.length.odd?
    str[middle_index]
  else
    str[middle_index -1.. middle_index]
  end
end

# Return the number of vowels in a string.
def num_vowels(str)
  vowels = "aeiou"
  str.chars.count {|letter| vowels.include?(letter)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ''
  arr.each_index do |i|
    joined << arr[i]
    joined << separator unless i == arr.length - 1
  end
  joined
end


# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |ch, i|
    if i.odd?
      ch.upcase
    else
      ch.downcase
    end
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  words.map do |word|
    if word.length > 4
      word.reverse
    else
      word
    end
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzed = []

  (1..n).each do |el|
    if el%3 == 0 && el%5 == 0
      fizzed << "fizzbuzz"
  elsif el%3 == 0
     fizzed << "fizz"
  elsif el%5 == 0
     fizzed << "buzz"
  else
     fizzed << el
  end
  end
  fizzed
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []

    arr.each do |el|
      new_arr.unshift(el)
    end
    new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num/2).each do |n|
    if num%n == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factored = []
  (1..num).each do |n|
    factored << n if num % n == 0
  end
  factored
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|num| prime?(num)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = arr.count {|num| num.odd?}
  even = arr.count {|num| num.even?}
  if odd == 1
     arr.select {|num| num.odd?}.join.to_i
   else
    arr.select {|num| num.even?}.join.to_i
  end
end
